
package ukol1;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.*;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

public class GateClient {
    
    // corpus pipeline
    private static SerialAnalyserController annotationPipeline = null;
    
    // whether the GATE is initialised
    private static boolean isGateInitilised = false;
    
    String title;
    String description;

    String h1;
    String h2;
    String h3;
    String p;
    
    /**
     * Bublinkove razeni
     * @param array pole k serazeni
     * @return 
     */
    public static Word[] bubbleSort(Word[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) { //postupne redukujeme problem
                if (array[j].count > array[j + 1].count) { //pokud je vyssi prvek spatne zarazen
                    Word tmp = array[j]; //tak jej prohodime
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
        return array;
    }
    
    public void run() throws IOException{
        
        String url; 
        int statusCode;
        boolean valid = false;
        org.jsoup.nodes.Document html;
        
        do {
            Scanner in = new Scanner(System.in);  
            System.out.println("Enter URL address: ");  
            url = in.nextLine();  
            if (!url.startsWith("http://") ) {
                url = "http://" + url;
            }
            //System.out.println("You entered URL address " + url); 

            Connection.Response response = Jsoup.connect(url)
                .timeout(10000)
                .execute();
            statusCode = response.statusCode();
            if (statusCode == 200) {
                valid = true;
            }
        } while (!valid);
        
        html = Jsoup.connect(url).get();
        
//        org.jsoup.nodes.Document html = Jsoup.connect("http://www.bbc.com/news/world/europe/").data("name", "jsoup").get();
//        org.jsoup.nodes.Document html = Jsoup.connect("http://en.wikipedia.org/wiki/Scotland").data("name", "jsoup").get();
//        org.jsoup.nodes.Document html = Jsoup.connect("http://www.bbc.com/news/science-environment-31965456").data("name", "jsoup").get();
        
        title = html.title();
        Element meta = html.select("meta[name$=description]").first();
        if (meta != null) description = meta.attr("content");
        
        h1 = html.select("h1").text();
        h2 = html.select("h2").text();
        h3 = html.select("h3").text();
        p = html.select("p").text();
        
        System.out.println("Title = " + title);
        System.out.println("Description = " + description);
        System.out.println("H1 = " + h1);
        System.out.println("H2 = " + h2);
        System.out.println("H3 = " + h3);
        System.out.println("p = " + p);
        
        if(!isGateInitilised){
            
            // initialise GATE
            initialiseGate();            
        }        

        try {                
            // create an instance of resources
            ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR");
            ProcessingResource englishTokenizer = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");
            ProcessingResource sentenceSplitter = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter");
            ProcessingResource posTagger = (ProcessingResource) Factory.createResource("gate.creole.POSTagger");
            
            // locate the JAPE grammar file
            File japeOrigFile = new File("D:/CVUT-Magisterske/DDW-dolovani-dat-z-webu/ukol-1/DDW-ukol1/ukol-1.jape");
            java.net.URI japeURI = japeOrigFile.toURI();
            
            // create feature map for the transducer
            FeatureMap transducerFeatureMap = Factory.newFeatureMap();
            try {
                // set the grammar location
                transducerFeatureMap.put("grammarURL", japeURI.toURL());
                // set the grammar encoding
                transducerFeatureMap.put("encoding", "UTF-8");
            } catch (MalformedURLException e) {
                System.out.println("Malformed URL of JAPE grammar");
                System.out.println(e.toString());
            }
            
            // create an instance of a JAPE Transducer processing resource
            ProcessingResource japeTransducerPR = (ProcessingResource) Factory.createResource("gate.creole.Transducer", transducerFeatureMap);

            // create corpus pipeline
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

            // add the processing resources (modules) to the pipeline
            annotationPipeline.add(documentResetPR);
            annotationPipeline.add(englishTokenizer);
            annotationPipeline.add(sentenceSplitter);
            annotationPipeline.add(posTagger);
            annotationPipeline.add(japeTransducerPR);
            
            // create a document
            Document document = Factory.newDocument(title + " " + title + " " + title + " " + description + " " + description + " " + h1 + " " + h1 + " " + h2 + " " + h2 + " " + h3 + " " + p);
            // create a corpus and add the document
            Corpus corpus = Factory.newCorpus("");
            corpus.add(document);

            // set the corpus to the pipeline
            annotationPipeline.setCorpus(corpus);
            //run the pipeline
            annotationPipeline.execute();

            Document doc = corpus.get(0);

            // get the default annotation set
            AnnotationSet as_default = doc.getAnnotations();

            FeatureMap futureMap = null;
            // get all Token annotations
            AnnotationSet annSetTokens = as_default.get("CleanWords",futureMap);
            System.out.println("\nNumber of Tokens: " + annSetTokens.size());

            ArrayList lookupAnnotations = new ArrayList(annSetTokens);

            Word[] words;
            words = new Word[lookupAnnotations.size()];
            
            for (int h = 0; h < lookupAnnotations.size(); ++h) {
                Annotation token = (Annotation)lookupAnnotations.get(h);
                String word = doc.getContent().getContent(token.getStartNode().getOffset(), token.getEndNode().getOffset()).toString();
                
                words[h] = new Word(h, word, 1);
            }
            // iterate through the tokens
            for (int i = 0; i < lookupAnnotations.size(); ++i) {
                for (int j = (i+1); j < lookupAnnotations.size(); ++j) {
                    if (words[i].word.equals(words[j].word) && words[i].count != 0) {
                        words[i].count++;
                        words[j].count = 0;
                    }
                }
            }
            
            words = bubbleSort(words);

            for (int k = lookupAnnotations.size()-1; k > lookupAnnotations.size()-20; k--) {
                if (words[k].count > 1) {
                    System.out.print(words[k].word + "(" + words[k].count + "), ");
                }
            }
            
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initialiseGate() {
        
        try {
            // set GATE home folder
            // Eg. /Applications/GATE_Developer_7.0
            File gateHomeFile = new File("C:/Program Files/GATE_Developer_8.0");
            Gate.setGateHome(gateHomeFile);
            
            // set GATE plugins folder
            // Eg. /Applications/GATE_Developer_7.0/plugins            
            File pluginsHome = new File("C:/Program Files/GATE_Developer_8.0/plugins");
            Gate.setPluginsHome(pluginsHome);            
            
            // set user config file (optional)
            // Eg. /Applications/GATE_Developer_7.0/user.xml
            Gate.setUserConfigFile(new File("C:/Program Files/GATE_Developer_8.0", "user.xml"));            
            
            // initialise the GATE library
            Gate.init();
            
            // load ANNIE plugin
            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURL();
            register.registerDirectories(annieHome);
            
            // flag that GATE was successfuly initialised
            isGateInitilised = true;
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }      
}